<?php
/**
 * @package     Aesir.plugin
 * @subpackage  User.Sponsorer
 *
 * @copyright   Copyright (C) 2008 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC');

/**
 * PlgUserSponsorer
 *
 * @since 1.0
 */
class PlgUserSponsorer extends JPlugin
{
	/**
	 * onUserLogin
	 *
	 * @param   mixed  $options  Option
	 *
	 * @return  bool
	 *
	 * @throws  Exception
	 */
	public function onUserAfterLogin($options)
	{
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		if (!$app->isSite())
		{
			return true;
		}

		if (!$user)
		{
			throw new Exception('No user was found.');
		}

		if ($this->params->get('testMode', 0) == 1)
		{
			if ($this->params->get('domain', false) && substr(strrchr($user->email, "@"), 1) == $this->params->get('domain', false))
			{
				$app->redirect(JRoute::_('index.php?option=com_reditem&view=itemdetail&id=' . $this->params->get('itemID', false) . '&Itemid=156'), false);
				$app->close();

				return;
			}
		}

		$db = JFactory::getDbo();

		$id = JUserHelper::getUserId($user->username);

		$remote_key = $db->setQuery(
			$db->getQuery(true)
				->select('remote_key')
				->from('#__aesir_sync_sync')
				->where($db->qn('reference') . ' = ' . $db->q('sponsor.user'))
				->where($db->qn('local_id') . ' = ' . $db->q($id))
		)->loadResult();

		if (!$remote_key)
		{
			return true;
		}

		$item_id = $db->setQuery(
			$db->getQuery(true)
				->select('local_id')
				->from('#__aesir_sync_sync')
				->where($db->qn('reference') . ' = ' . $db->q('sponsor'))
				->where($db->qn('remote_key') . ' = ' . $db->q($remote_key))
		)->loadResult();

		if (!$item_id)
		{
			return true;
		}

		$app->redirect(JRoute::_('index.php?option=com_reditem&view=itemdetail&id=' . $item_id . '&Itemid=156'), false);

		$app->close();

		return true;
	}
}
