<?php
/**
 * @package     Aesir.plugin
 * @subpackage  User.Sponsorer
 *
 * @copyright   Copyright (C) 2008 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC');

/**
 * PlgSystemSponsorer
 *
 * @since 1.0
 */
class PlgSystemSponsorer extends JPlugin
{
	/**
	 * onAfterRender
	 *
	 * @return  void
	 */
	public function onAfterRender()
	{
		$user   = JFactory::getUser();
		$app    = JFactory::getApplication();

		$this->smartLink($app, $user);
		$this->smartCheck($app, $user);

		return true;
	}

	/**
	 * smartCheck
	 *
	 * @param   JApplication  $app   Application
	 * @param   JUser         $user  User
	 *
	 * @return bool
	 */
	public function smartCheck($app, JUser $user)
	{
		if (!$app->isSite())
		{
			return false;
		}

		if ($app->input->get('option') == 'com_reditem' && $app->input->get('view', '') == 'itemdetail')
		{
			/** Make all redweb mail testing all client sites */
			if ($user->guest == 0 && $this->params->get('testMode', 0) == 1)
			{
				if ($this->params->get('domain', false) && substr(strrchr($user->email, "@"), 1) == $this->params->get('domain', false))
				{
					return false;
				}
			}

			$db     = JFactory::getDbo();
			$itemID = (int) $app->input->get('id', 0);

			$remote_key = $db->setQuery(
				$db->getQuery(true)
					->select('remote_key')
					->from('#__aesir_sync_sync')
					->where($db->qn('reference') . ' = ' . $db->q('sponsor'))
					->where($db->qn('local_id') . ' = ' . $db->q($itemID))
			)->loadResult();

			if (empty($remote_key))
			{
				return false;
			}
			elseif ($user->guest == 1)
			{
				$app->redirect("index.php?error=1003");

				return false;
			}

			$local_user_id = $db->setQuery(
				$db->getQuery(true)
					->select('local_id')
					->from('#__aesir_sync_sync')
					->where($db->qn('reference') . ' = ' . $db->q('sponsor.user'))
					->where($db->qn('remote_key') . ' = ' . $db->q($remote_key))
			)->loadResult();

			if ($local_user_id != $user->id)
			{
				$app->redirect("index.php?error=1002");

				return false;
			}

			return true;
		}
	}

	/**
	 * smartLink
	 *
	 * @param   JApplication  $app   Application
	 * @param   JUser         $user  User
	 *
	 * @return bool|void
	 */
	public function smartLink($app, JUser $user)
	{
		if (!$app->isSite())
		{
			return false;
		}

		if ($app->input->get('_task', '') != 'sponsorer.go')
		{
			return false;
		}

		if (!$user)
		{
			$app->redirect('index.php');
		}

		if ($this->params->get('testMode', 0) == 1)
		{
			if ($this->params->get('domain', false) && substr(strrchr($user->email, "@"), 1) == $this->params->get('domain', false))
			{
				$app->redirect(JRoute::_(('index.php?option=com_reditem&view=itemdetail&id=' . $this->params->get('itemID', false)) . '&Itemid=156'), false);
				$app->close();

				return;
			}
		}

		$db = JFactory::getDbo();

		$id = JUserHelper::getUserId($user->username);

		$remote_key = $db->setQuery(
			$db->getQuery(true)
				->select('remote_key')
				->from('#__aesir_sync_sync')
				->where($db->qn('reference') . ' = ' . $db->q('sponsor.user'))
				->where($db->qn('local_id') . ' = ' . $db->q($id))
		)->loadResult();

		if (!$remote_key)
		{
			$app->redirect('index.php?error=1001');
		}

		$item_id = $db->setQuery(
			$db->getQuery(true)
				->select('local_id')
				->from('#__aesir_sync_sync')
				->where($db->qn('reference') . ' = ' . $db->q('sponsor'))
				->where($db->qn('remote_key') . ' = ' . $db->q($remote_key))
		)->loadResult();

		if (!$item_id)
		{
			$app->redirect('index.php?error=1002');
		}

		$app->redirect(JRoute::_('index.php?option=com_reditem&view=itemdetail&id=' . $item_id . '&Itemid=156'), false);

		$app->close();
	}
}
