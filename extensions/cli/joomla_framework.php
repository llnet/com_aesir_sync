<?php
/**
 * @package     AesirSync.Cli
 * @subpackage  Joomla Required
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later, see LICENSE.
 */

const _JEXEC = 1;
define('_DIR_', realpath(dirname(__DIR__) . '/../'));

// Load system defines
if (file_exists(_DIR_ . '/defines.php'))
{
    require_once dirname(_DIR_) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', _DIR_);
    require_once JPATH_BASE . '/includes/defines.php';
}

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Import the configuration.
require_once JPATH_CONFIGURATION . '/configuration.php';

define('JDEBUG', 0);

$_SERVER['REQUEST_METHOD'] = 'GET';