<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Controller
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Base Controller.
 *
 * @package     Aesir_Sync.Backend
 * @subpackage  Controller
 * @since       1.0.0
 */
class Aesir_SyncController extends JControllerLegacy
{
	/**
	 * Typical view method for MVC based architecture
	 *
	 * This function is provide as a default implementation, in most cases
	 * you will need to override it in your own controllers.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   array    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JControllerLegacy  A JControllerLegacy object to support chaining.
	 */
	public function display($cachable = false, $urlparams = array())
	{
		$input = JFactory::getApplication()->input;
		$input->set('view', $input->getCmd('view', 'sync'));
		$input->set('task', $input->getCmd('task', 'display'));

		return parent::display($cachable, $urlparams);
	}
}
