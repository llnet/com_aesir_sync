SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;


-- -----------------------------------------------------
-- Table `#__aesir_sync_cron`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_cron` ;

CREATE TABLE IF NOT EXISTS `#__aesir_sync_cron` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `parent_id` INT(11) NULL DEFAULT '0',
  `state` TINYINT(4) NOT NULL DEFAULT '0',
  `start_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `next_start` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lft` INT(11) NOT NULL,
  `rgt` INT(11) NOT NULL,
  `level` INT(10) UNSIGNED NOT NULL,
  `alias` VARCHAR(255) NOT NULL,
  `path` VARCHAR(255) NOT NULL,
  `parent_alias` VARCHAR(255) NOT NULL DEFAULT 'root',
  `execute_sync` TINYINT(4) NOT NULL,
  `mask_time` VARCHAR(255) NOT NULL DEFAULT 'Y-m-d 00:00:00',
  `offset_time` VARCHAR(255) NOT NULL DEFAULT '+1 day',
  `params` TEXT NOT NULL,
  `checked_out` INT(11) NOT NULL DEFAULT '0',
  `checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  INDEX `idx_common` (`next_start` ASC, `state` ASC),
  INDEX `idx_parent_id` (`parent_id` ASC),
  INDEX `idx_left_right` (`lft` ASC, `rgt` ASC),
  INDEX `idx_name` (`name` ASC),
  UNIQUE INDEX `idx_alias` (`alias` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `#__aesir_sync_sync`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_sync` ;

CREATE TABLE IF NOT EXISTS `#__aesir_sync_sync` (
  `reference` VARCHAR(100) NOT NULL,
  `remote_key` VARCHAR(255) NOT NULL,
  `remote_parent_key` VARCHAR(100) NOT NULL,
  `local_id` VARCHAR(100) NOT NULL,
  `execute_sync` TINYINT(4) NOT NULL DEFAULT '0',
  `main_reference` TINYINT(1) NOT NULL DEFAULT '0',
  `deleted` TINYINT(1) NOT NULL DEFAULT '0',
  `serialize` TEXT NOT NULL,
  `metadata` TEXT NULL,
  PRIMARY KEY (`reference`, `remote_key`, `local_id`, `remote_parent_key`, `execute_sync`),
  INDEX `idx_reference` (`reference` ASC),
  INDEX `idx_remote_key` (`remote_key` ASC),
  INDEX `idx_local_id` (`local_id` ASC),
  INDEX `idx_remote_parent_key` (`remote_parent_key` ASC),
  INDEX `idx_execute_sync` (`execute_sync` ASC),
  INDEX `idx_main_reference` (`main_reference` ASC),
  INDEX `idx_deleted` (`deleted` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `#__aesir_sync_config`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_config` ;

CREATE TABLE IF NOT EXISTS `#__aesir_sync_config` (
  `key` VARCHAR(255) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`key`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



-- -----------------------------------------------------
-- Table `#__aesir_sync_methods`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_methods` ;

CREATE TABLE `#__aesir_sync_methods` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `sequence_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `#__aesir_sync_methods`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `#__aesir_sync_methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



-- -----------------------------------------------------
-- Table `#__aesir_sync_setup`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_setup` ;

CREATE TABLE `#__aesir_sync_setup` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `url_id` int(4) NOT NULL,
  `download` tinyint(1) NOT NULL,
  `unfold` tinyint(1) NOT NULL,
  `not_overwrite` int(11) NOT NULL,
  `thekey` int(11) NOT NULL,
  `thevalue` int(11) NOT NULL,
  `main` int(11) NOT NULL,
  `map` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `#__aesir_sync_setup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE `#__aesir_sync_setup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



-- -----------------------------------------------------
-- Table `#__aesir_sync_setup_methods`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_setup_methods` ;

CREATE TABLE `#__aesir_sync_setup_methods` (
  `id` int(11) NOT NULL,
  `setup_id` int(11) NOT NULL,
  `method_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `#__aesir_sync_setup_methods`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `#__aesir_sync_setup_methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



-- -----------------------------------------------------
-- Table `#__aesir_sync_statistics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_statistics` ;

CREATE TABLE `#__aesir_sync_statistics` (
  `id` int(11) NOT NULL,
  `mark` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- -----------------------------------------------------
-- Table `#__aesir_sync_url`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_url` ;

CREATE TABLE `#__aesir_sync_url` (
  `id` int(11) NOT NULL,
  `source` varchar(200) NOT NULL,
  `name` varchar(40) NOT NULL,
  `setup_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `#__aesir_sync_url`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `#__aesir_sync_url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;





SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
