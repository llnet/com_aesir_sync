SET FOREIGN_KEY_CHECKS = 0;

INSERT INTO `#__aesir_sync_cron` (`id`, `name`, `parent_id`, `state`, `start_time`, `finish_time`, `next_start`, `lft`, `rgt`, `level`, `alias`, `path`, `parent_alias`, `execute_sync`, `mask_time`, `offset_time`, `params`, `checked_out`, `checked_out_time`) VALUES
	(0, 'ROOT', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 'root', '', '', 0, '', '', '', 0, '0000-00-00 00:00:00');

INSERT INTO `#__aesir_sync_cron` (`name`, `state`, `start_time`, `finish_time`, `next_start`, `alias`, `parent_alias`, `execute_sync`, `mask_time`, `offset_time`, `params`, `checked_out`, `checked_out_time`) VALUES
	('GetVenues', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'getvenues', 'root', 0, 'Y-m-d H:00:00', '+1 hour', '', 0, '0000-00-00 00:00:00'),
	('GetShows', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'getshows', 'getvenues', 0, 'Y-m-d H:00:00', '+1 hour', '', 0, '0000-00-00 00:00:00'),
	('GetSponsors', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'getsponsors', 'root', 0, 'Y-m-d H:00:00', '+1 hour', '', 0, '0000-00-00 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
