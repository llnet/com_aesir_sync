<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Tables.Cron
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die;

/**
 * Cron table.
 *
 * @package     Aesir_Sync.Backend
 * @subpackage  Tables.Cron
 * @since       1.0.0
 */
class Aesir_SyncTableCron extends RTableNested
{
	/**
	 * The table name without the prefix.
	 *
	 * @var  string
	 */
	protected $_tableName = 'aesir_sync_cron';
}
