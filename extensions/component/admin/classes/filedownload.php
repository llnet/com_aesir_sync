<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.FileDownload
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

class FileDownload
{
	/**
	 * @var Buffer
	 */
	public $downloadBuffer;

	/**
	 * @var Queue
	 */
	public $imageQueue;

	/**
	 * @var integer
	 * @ToDo Change name to index maybe.
	 */
	public $i = 0;

	/**
	 * @var resource
	 */
	public $multi;

	/**
	 * @var array
	 */
	public $curls = array();

	/**
	 * @var array
	 */
	public $options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_MAXREDIRS      => 5
	);

	/**
	 * @var integer
	 */
	public $buffer_size = 5;

	/**
	 * @var integer
	 */
	public $ratio = 10000;

	/**
	 * @var boolean
	 */
	public $finished = false;

	/**
	 * FileDownload constructor.
	 */
	public function __construct()
	{
		$this->downloadBuffer = new Buffer($this->buffer_size);
		$this->imageQueue     = new Queue(1000000);
		$this->multi           = curl_multi_init();
	}

	/**
	 * download
	 *
	 * @param   string  $url  Url
	 *
	 * @return  void
	 */
	public function download($url)
	{
		$this->image_queue->push($url);
	}

	/**
	 * clock
	 *
	 * @return integer
	 */
	public function clock()
	{
		$this->queueToBuffer();

		return $this->poke();
	}

	/**
	 * queueToBuffer
	 *
	 * @return void
	 */
	public function queueToBuffer()
	{
		if (!$this->imageQueue->isEmpty && !$this->downloadBuffer->isFull)
		{
			$url    = $this->imageQueue->pull();
			$handle = curl_init($url);
			curl_setopt_array($handle, $this->options);

			/**
			 * You need to fix this
			 */
			$file = fopen("/Applications/MAMP/htdocs/testbilleder/" . ($this->i++) . "." . explode("?", pathinfo($url)["extension"])[0], "w");

			$this->downloadBuffer->insert($handle, $file);
			curl_setopt($handle, CURLOPT_FILE, $file);
			curl_multi_add_handle($this->multi, $handle);
		}
	}

	/**
	 * removeFinished
	 *
	 * @param   mixed  $handle  Mixed
	 *
	 * @return  void
	 */
	public function removeFinished($handle)
	{
		$file = $this->downloadBuffer->remove($handle);
		fclose($file);
		curl_multi_remove_handle($this->multi, $handle);
	}

	/**
	 * poke
	 *
	 * @return integer
	 */
	public function poke()
	{
		for ($a = 0; $a < $this->ratio; $a++)
		{
			$execrun = curl_multi_exec($this->multi, $running);

			if ($execrun != CURLM_OK)
			{
				return -1;
			}
			elseif (!$running)
			{
				return 1;
			}

			$done = curl_multi_info_read($this->multi);

			if ($done)
			{
				$info = curl_getinfo($done['handle']);

				if ($info['http_code'] == 200)
				{
					$this->removeFinished($done["handle"]);
				}
				else
				{
					return -2;
				}
			}
		}

		return 0;
	}

}
