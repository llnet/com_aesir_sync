<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.Helper
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

class Helper 
{
	/**
	 * reverse
	 *
	 * @param   string  $data   Data
	 * @param   mixed   $path   Path
	 * @param   mixed   $depth  Depth
	 *
	 * @return string
	 */
	public static function reverse($data,$path = null,$depth = null)
	{
		return strrev($data);
	}

	/**
	 * upper
	 *
	 * @param   string  $data   Data
	 * @param   mixed   $path   Path
	 * @param   mixed   $depth  Depth
	 *
	 * @return string
	 */
	public static function upper($data, $path = null,$depth = null)
	{
		return strtoupper($data);
	}

	/**
	 * upperMedDato
	 *
	 * @param   string  $data   Data
	 * @param   mixed   $path   Path
	 * @param   mixed   $depth  Depth
	 *
	 * @return string
	 */
	public static function upperMedDato($data,$path = null,$depth = null)
	{
		return strtoupper($data) . "(dato=" . ($path[($depth - 1)][1]["date"]) . ")";
	}

}
