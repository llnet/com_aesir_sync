<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.Buffer
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

/**
 * Class Buffer
 *
 * @since  1.0.0
 */
class Buffer
{
	/**
	 * @var array
	 */
	public $elements = array();

	/**
	 * @var mixed
	 */
	public $size;

	/**
	 * @var boolean
	 */
	public $isEmpty = true;

	/**
	 * @var boolean
	 */
	public $isFull = false;

	/**
	 * Buffer constructor.
	 *
	 * @param   mixed  $size  Size
	 */
	public function __construct($size)
	{
		$this->size = $size;
	}

	/**
	 * insert
	 *
	 * @param   string  $key      Key Element
	 * @param   mixed   $element  Element
	 *
	 * @return boolean
	 */
	public function insert($key, $element)
	{
		if (count($this->elements) < $this->size)
		{
			$this->elements[$key] = $element;
			$this->isEmpty        = false;

			if (count($this->elements) == $this->size)
			{
				$this->isFull = true;
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * remove
	 *
	 * @param   string  $key  Key
	 *
	 * @return  mixed
	 */
	public function remove($key)
	{
		$element = $this->elements[$key];
		unset($this->elements[$key]);
		$this->isFull = false;

		if (count($this->elements) == 0)
		{
			$this->isEmpty = true;
		}

		return $element;
	}

}
