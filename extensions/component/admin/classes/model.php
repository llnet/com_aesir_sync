<?php

//namespace AesirSync;

defined('_JEXEC') or die;

/**
 * Model sync class
 *
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.Base
 * @since       1.0.0
 */
class Model
{
	/**
	 * @var integer
	 *
	 * @todo better name
	 */
	public static $i = 0;

	/**
	 * insertSetupDB
	 *
	 * @param   integer $parent Parent
	 * @param   string  $name   Name
	 * @param   string  $path   Path
	 *
	 * @return mixed
	 */
	public static function insertSetupDB($parent = null, $name = null, $path = null)
	{
		return self::runSQL(
			"insert into #__aesir_sync_xml(parent_id,name,type,path,datatype)" .
			"values (" . $parent . ",'" . $name . "','xml','" . $path . "', 'text')",
			"insertid"
		);
	}

	/**
	 * readSetupDB
	 *
	 * @param   integer $parent Parent
	 *
	 * @return  mixed
	 */
	public static function readSetupDB($parent)
	{
		return self::runSQL(
			("select * from #__aesir_sync_xml where parent_id=" . (int) $parent),
			"loadAssocList"
		);
	}

	/**
	 * readURLDB
	 *
	 * @param   string $name Name
	 *
	 * @return mixed
	 */
	public static function readURLDB($name)
	{
		return self::runSQL(
			"select * from #__aesir_sync_xml where name='" . $name . "'",
			"loadAssocList"
		);
	}

	/**
	 * readUnfoldDB
	 *
	 * @param   mixed $parent   Parent
	 * @param   mixed $keyValue Keyvalue
	 *
	 * @return mixed
	 */
	public static function readUnfoldDB($parent, $keyValue)
	{
		return self::runSQL(
			"select name from #__aesir_sync_xml where parent_id=" . $parent . " and " . $keyValue . "=1",
			"loadResult"
		);
	}

	/**
	 * readMethodsDB
	 *
	 * @param   integer $id ID
	 *
	 * @return mixed
	 */
	public static function readMethodsDB($id)
	{
		return self::runSQL(
			"select name from #__aesir_sync_setup_methods as amsm " .
			"join #__aesir_sync_methods as amm " .
			"on amsm.method_id = amm.id " .
			"where setup_id=" . $id . " order by amsm.sequence_number",
			"loadColumn"
		);
	}

	/**
	 * readAllMethodsDB
	 *
	 * @return mixed
	 */
	public static function readAllMethodsDB()
	{
		return self::runSQL(
			"select name from #__aesir_sync_methods",
			"loadColumn"
		);
	}

	/**
	 * runSQL
	 *
	 * @param   mixed $query  Query
	 * @param   mixed $method Method
	 *
	 * @return mixed
	 */
	public static function runSQL($query, $method)
	{
		$db = JFactory::getDbo();
		$db->setQuery($query);

		if ($method == "insertid")
		{
			$db->execute();
		}

		return $db->$method();
	}

	/**
	 * getSynchronize
	 *
	 * @return mixed
	 */
	public static function getSynchronize()
	{
		return self::getTree(
			null,
			1
		);
	}

	/**
	 * getBranch
	 *
	 * @param   integer $id Id
	 *
	 * @return  array
	 */
	public static function getBranch($id)
	{
		return array(
			$id => array(
				"id"       => $id,
				"children" => self::getTree(null, 0, $id)
			)
		);
	}

	/**
	 * getTree
	 *
	 * @param   mixed   $input Input
	 * @param   integer $sync  Sync
	 * @param   integer $id    Id
	 *
	 * @return mixed
	 */
	public static function getTree($input, $sync = 0, $id = 0)
	{
		$sql = "select " .
			"*, " .
			"(select count(*)=0 from itp2i_aesir_sync_xml as t2 where t1.id=t2.parent_id) as leaf, " .
			"(select GROUP_CONCAT(name) from itp2i_aesir_sync_methods as t3 " .
			"join itp2i_aesir_sync_setup_methods as t4 on t4.method_id=t3.id " .
			"where t4.setup_id=t1.id) as methods " .
			"from itp2i_aesir_sync_xml as t1";

		if ($sync)
		{
			$sql .= " where t1.sync=1 or t1.parent_id=0";
		}

		/** @Todo i have change to self */
		$all = self::runSQL($sql, "loadAssocList");

		$allByParent = array();

		foreach ($all as $value)
		{
			$allByParent[$value["parent_id"]][] = $value;
		}

		self::getTree2(
			$allByParent[$id],
			$allByParent,
			$tree
		);

		return $tree;
	}

	/**
	 * getTree2
	 *
	 * @param   mixed $elements    Elements
	 * @param   mixed $elementsOrg Elements Org
	 * @param   mixed $tree        Tree
	 *
	 * @return void
	 */
	public static function getTree2($elements, &$elementsOrg, &$tree)
	{
		foreach ($elements as $key => $value)
		{
			if ($elementsOrg[$value["id"]])
			{
				$value["children"] = [];
			}

			$tree[$value["id"]] = $value;

			if ($elementsOrg[$value["id"]])
			{
				self::getTree2(
					$elementsOrg[$value["id"]],
					$elementsOrg,
					$tree[$value["id"]]["children"]
				);
			}
		}
	}

	/**
	 * deleteXML
	 *
	 * @param   mixed $input Input
	 *
	 * @return void
	 */
	public static function deleteXML($input)
	{
		/**
		 * @deprecated not used code
		 */
		$a = self::runSQL(
			("select reference from #__aesir_sync_xml where id=" . $input["id"]),
			"loadResult"
		);

		self::deleteXMl_2(
			self::getBranch(
				(int) $input["id"]
			)
		);
	}

	/**
	 * deleteXML_2
	 *
	 * @param   array $node Node
	 *
	 * @todo  missing static
	 */
	public static function deleteXML_2($node)
	{
		foreach ($node as $value)
		{
			self::runSQL(("delete from #__aesir_sync_xml where id=" . $value["id"]), "execute");

			/**
			 * @todo remember check if data is right
			 */
			if (isset($value["children"]) && (is_array($value["children"]) || is_object($value["children"])))
			{
				self::deleteXML_2($value["children"]);
			}
		}
	}

	/**
	 * addXML
	 *
	 * @param   mixed $input Input
	 *
	 * @return  mixed
	 */
	public function addXML($input)
	{
		self::runSQL(
			"insert into #__aesir_sync_xml(parent_id,name,type,url,content_type)" .
			" values(" . $input["id"] . ",'new XML','url','http://address_to_xml','xml')",
			"execute"
		);

		/**
		 * @Todo - this will return error/warnings
		 */
		return self::getTree();
	}

	/**
	 * setMethods
	 *
	 * @param   mixed $input Input
	 *
	 * @return  void
	 */
	public function setMethods($input)
	{
		self::runSQL("delete from #__aesir_sync_setup_methods where setup_id=" . $input["id"], "execute");

		foreach (explode(",", $input["value"]) as $value)
		{
			self::runSQL("insert into #__aesir_sync_setup_methods(setup_id,method_id) " .
				"values(" . $input["id"] . ",(select id from #__aesir_sync_methods where name='" . $value . "'))",
				"execute"
			);
		}
	}

	/**
	 * @param $input
	 *
	 * @return mixed
	 */
	public function setOption($input)
	{
		return self::runSQL("update #__aesir_sync_xml set " . $input["key"] . "='" . $input["value"] . "' where id=" . $input["id"], "execute");
	}

	/**
	 * loadXML
	 *
	 * @param   mixed  $input  Input
	 *
	 * @return mixed
	 */
	public function loadXML($input)
	{
		self::runSQL(
			"update #__aesir_sync_xml set name='" . $input["name"] . "', url='" . base64_decode($input["url"]) . "', " .
			"content_type='" . $input["content_type"] . "' where id=" . $input["id"],
			"execute"
		);

		$t = new XMLImport($input["id"]);
		$t->generate();

		/**
		 * @Todo - this will return error/warnings
		 */
		return self::getTree();
	}

	/**
	 * syncAll
	 *
	 * @param   $input
	 *
	 * @return  string
	 */
	public function syncAll($input)
	{
		/** @Todo Problem !! localhost setup in code */
		$output = shell_exec("php -c /Applications/MAMP/bin/php/php7.0.13/conf/php.ini " . JPATH_ROOT . "/cli/com_aesir_sync/cron.php");

		return $output;
	}

	/**
	 * splitDateTime
	 *
	 * @param   mixed  $datetime    DateTime
	 * @param   mixed  $substitute  SubsTitute
	 *
	 * @return array
	 */
	public static function splitDateTime($datetime, $substitute)
	{
		$output = array();
		$t      = new DateTime($datetime);

		foreach (array("H", "i", "m", "d", "w", "D", "N") as $value)
		{
			$output[$value] = $t->format($value);
		}

		$output = array_merge($output, $substitute);

		return $output;
	}

	/**
	 * @param $input
	 *
	 * @return string
	 */
	public static function addCron($input)
	{
		if ($input["datetime_repeat"] == "repeat same day every month")
		{
			$tx = self::splitDateTime($input["datetime"], array("m" => "*", "N" => "*"));
		}
		elseif ($input["datetime_repeat"] == "repeat same day every week")
		{
			$tx = self::splitDateTime($input["datetime"], array("m" => "*", "d" => "*"));
		}

		$wx = ($tx["N"] != "*" ? ($tx["N"] % 7) : "*");

		/** @todo again some localhost path */
		$output = shell_exec("(crontab -l 2>/dev/null; echo '" . $tx["i"] . " " . $tx["H"] . " " . $tx["d"] . " " . $tx["m"] . " " . $wx . " " .
			"php /Applications/MAMP/htdocs/redweb/joomla7/cli/com_aesir_sync/cron.php') | crontab -");

		return $output;
	}

	/**
	 * removeCron
	 *
	 * @param   mixed  $input  Input
	 *
	 * @return  string
	 */
	public function removeCron($input)
	{
		$output = shell_exec("crontab -l | grep -v 'php /Applications/MAMP/htdocs/redweb/joomla7/cli/com_aesir_sync/cron.php' | crontab -");

		return $output;
	}

	/**
	 * removeCron
	 *
	 * @param   mixed  $input  Input
	 *
	 * @return  string
	 */
	public static function getProgress($input)
	{
		return self::runSQL("select (progress/main_count) as percent, running from #__aesir_sync_xml where id=" . $input["id"], "loadAssoc");
	}

// ------------------------------------------------------------	

	public function test($input)
	{
		//$t = new XMLImport($input["id"]);

		/*$method = "download".$t->setup["content_type"];
		$t->$method();

		$t->getStructure();*/

	}


}
