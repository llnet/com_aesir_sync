<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.Queue
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

/**
 * Class Queue
 *
 * @since  1.0.0
 */
class Queue
{
	/**
	 * @var array
	 */
	public $elements = [];

	/**
	 * @var integer
	 */
	public $size = 0;

	/**
	 * @var integer
	 */
	public $head = 0;

	/**
	 * @var integer
	 */
	public $tail = 0;

	/**
	 * @var boolean
	 */
	public $isFull = false;

	/**
	 * @var boolean
	 */
	public $isEmpty = true;

	/**
	 * Queue constructor.
	 *
	 * @param   mixed $size Size
	 */
	public function __construct($size)
	{
		$this->size = $size;
	}

	/**
	 * push
	 *
	 * @param   mixed $element Element
	 *
	 * @return void
	 */
	public function push(&$element)
	{
		if (!$this->isFull)
		{
			$this->elements[$this->head] = $element;
			$this->isEmpty               = false;
			$this->head                  = ($this->head + 1) % $this->size;

			if ($this->head == $this->tail)
			{
				$this->isFull = true;
			}
		}
	}

	/**
	 * pull
	 *
	 * @return  mixed
	 */
	public function pull()
	{
		if (!$this->isEmpty)
		{
			$this->isFull                = false;
			$return                      = $this->elements[$this->tail];
			$this->elements[$this->tail] = "";
			$this->tail                  = ($this->tail + 1) % $this->size;

			if ($this->head == $this->tail)
			{
				$this->isEmpty = true;
			}

			return $return;
		}

		return null;
	}
}
