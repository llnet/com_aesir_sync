
	var methods = ['pop last method','reverse','upper','upperMedDato','fullname','grillbar','eifeltårn'];
	var b2i = {"true":1,"false":0};
	var b2show = {true:"inline",false:"hidden",0:"hidden",1:"inline"};
	var i2c = {1:"checked",0:""};
	var i2cc = {1:"btn-success",0:"btn-danger",true:"btn-success",false:"btn-danger"};
	var xmljson = ['xml','json','csv'];
	var datetime_choices = ['','repeat same day every month','repeat same day every week'];
  var tree_types = {
  	"root":function(element,depth) {
  		return 	"<p onclick=\"showhide('"+element.id+"','subtree_',this);\">"+pm+"</p>"+
              "<div class='inline' style='width:"+(first_field_width-depth*column_width)+"px;'>"+
  						"<span>"+element.name+"</span>"+
  						"<i class='marginleft fontsize1 green icon-plus' onclick='addXML("+element.id+");'></i></div>"+
  						//"<i class='marginleft orange fas fa-sync' onclick='syncAll()'></i>"+
  						"<input id='datetime_"+element.id+"' type='datetime-local' class='margin-right' >"+
  						"<select id='datetime_repeat_"+element.id+"' class='marginleft'  >"+selectContent(datetime_choices,element.datetime_repeat)+"</select>"+
  						"<button type='button' class='btn btn-mini' onclick='addCron("+element.id+")'>set schedule</button>"+
  						"<button type='button' class='btn btn-mini' onclick='removeCron("+element.id+")'>clear schedule</button>"+
              "<button type='button' class='btn btn-mini' onclick='syncAll()'>sync now</button>";
  	},
  	"url":function(element,depth) {
  		return  "<p onclick=\"showhide('"+element.id+"','subtree_',this);\">"+
  						"<i class='blue fontsize1 icon-minus-sign'></i></p>"+
  						"<div class='inline' style='width:"+(first_field_width-depth*column_width)+"px;'>"+
  						"<span id='url_name_"+element.id+"' contenteditable='true' class='inline margin-right' >"+element.name+"</span>"+
  						"<i class='marginleft fontsize1 green icon-plus' onclick='addXML("+element.id+");'></i>"+
  						"<i class='marginleft fontsize1 red icon-trash' onclick='deleteXML("+element.id+");'></i></div>"+
  						"<button class='btn btn-mini marginleft "+i2cc[element.sync]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'sync')\">synchronize</button>"+
  						"<span class='margin-right' id='url_path_"+element.id+"' contenteditable='true' >"+element.url+"</span>"+
  						"<select id='content_type_"+element.id+"' class='marginleft' >"+selectContent(xmljson,element.content_type)+"</select>"+
  						"<button type='button' class='btn btn-mini' onclick='test("+element.id+")'>get setup</button>"+
  						"<button type='button' class='btn btn-mini' onclick='loadXML("+element.id+")'>create</button>"+
              "<p id='percent_"+element.id+"'></p>";

  	},
  	"xml_list":function(element,depth) {
  		return  "<span type='button' onclick=\"showhide('"+element.id+"','subtree_',this);\">"+
  							"<i class='turquoise fontsize1 "+leaf[element.leaf]+"'></i>"+
  						"</span>"+
  						"<p style='width:"+(first_field_width-depth*column_width)+"px;'>"+element.name+"</p>"+
  						"<button class='btn btn-mini "+i2cc[element.ignore_this]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'ignore_this')\">ignore</button>"+
  						"<button class='btn btn-mini "+i2cc[element.unfold]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'unfold')\">unfold</button>";
  						//"<p class='"+i2cc[element.main]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'main')\">main</p>";
  	},
  	"xml":function(element,depth) {
  		return  "<span type='button' onclick=\"showhide('"+element.id+"','subtree_',this);\">"+
  							"<i class='turquoise fontsize1 "+leaf[element.leaf]+"'></i>"+
  						"</span>"+
  						"<p style='width:"+(first_field_width-depth*column_width)+"px;'>"+element.name+"</p>"+
  						"<button class='btn btn-mini "+i2cc[element.ignore_this]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'ignore_this')\">ignore</button>"+
              "<button class='btn btn-mini "+i2cc[element.identifier]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'identifier')\">identifier</button>"+
  						"<button class='btn btn-mini "+i2cc[element.download]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'download')\">download</button>"+
  						//"<p class='"+i2cc[element.not_overwrite]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'not_overwrite')\">preserve</p>"+
  						"<button class='btn btn-mini "+i2cc[element.the_key]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'the_key')\">key</button>"+
  						"<button class='btn btn-mini "+i2cc[element.the_value]+" checkbox' onclick=\"toggleColor(this,"+element.id+",'the_value')\">value</button>"+
  						"<input type='text' id='map_"+element.id+"' class='text' title='map' onkeyup=\"addText(this,"+element.id+",'map',setText,this.value)\" value='"+element.map+"' >"+
  						"<select id='methods_"+element.id+"' class='select' onchange='addMethod(this,"+element.id+");' >"+methodsOptions(element)+"</select>";
  	}
  }
  var column_width = 15;
  var first_field_width = 270;
  var pm = "<i class='blue fontsize1 icon-minus-sign'></i>";
  var tree;
  var previous;
  var colapsed = {"xml":"none","url":"block","root":"block"};
  var leaf = {0:"icon-plus-sign",1:"icon-ban-circle disabled"};
  var previous_text;
  var timer = [];

  function addText(element,id,name,callback,text) {
  	if ( text == previous_text ) return; 
  	clearTimeout(timer[id]);
  	timer[id] = setTimeout(callback,1200,id,element,name,text);
  	previous_text = text;
  }

  function setText(id,element,name,text) {
  	clearTimeout(timer[id]);
  	setOption(id,{'key':name,'value':text});
  }

  function selectContent(array,selected) {
  	s = {true:'selected',false:''}
  	output = "";
  	for (value of array) output += "<option "+s[(selected==value)]+" >"+value;
  	return output;
  }

  function setContentType(element,id) {
  	setOption(id,{'key':'content_type','value':element.value},null);
  }

	function addMethod(element,id) {
		used = usedMethods(element.value)
		setMethods(id,{key:'methods',value:used.split("->").join(",")});
		element.innerHTML = availableMethods(used);
	}

	function usedMethods(used_methods) {
		in_use = used_methods.split("->");
		last = in_use.pop();
		if (last!="pop last method") in_use.push(last); 
		else in_use.pop();
		used_methods = in_use.join("->");
		return used_methods;
	}

	function availableMethods(used_methods) {
		in_use = used_methods.split("->");
		not_used = methods.filter(item => in_use.indexOf(item) == -1 );
		output = "<option>"+used_methods;
		if (used_methods!="") used_methods += "->";
		for (m of not_used) {
			output += "<option>"+used_methods + m;
		}
		return output;
	}

	function methodsOptions(element) {
		if (element.methods==null) element.methods = "";
		return availableMethods(element.methods.split(",").join("->"));
	}

  function showTree(elements,depth,output,path) {
  	for (var element of Object.values(elements) ) {
  		output.html += wrap(element,depth,element.type);
  		if ( element.children ) showTree(element.children,depth+1,output,path);
  		output.html += "</div>";
  	}
  	return output.html;
  }

  function wrap(element,depth,type) {
  	type = ( (element.leaf==0 && type=='xml')?'xml_list':type);
  	return 	"<div id='element_"+element.id+"' class='row-fluid ' style='padding-left:"+(depth*column_width)+"px;' onmouseover='switchBackg(this)' onmouseout='switchBackg(null);' >"+
  					//"<div class='span*' "+(depth*column_width)+";'></div>"+
						tree_types[type](element,depth)+"</div>"+
						"<div id='subtree_"+element.id+"' style='display:"+colapsed[element.type]+";' >";
  }

  function switchBackg(element) {
  	if (element==null || previous) previous.style.backgroundColor = '';
  	if (element!=null) element.style.backgroundColor = '#eeeeee';
  	previous = element;
  }

	function showhide(id,concat,from) {
		toggle = {'block':'none','none':'block'};
		temp = document.getElementById(concat+id);
		temp.style.display = toggle[temp.style.display];
		from.firstChild.classList.toggle('icon-minus-sign');
		from.firstChild.classList.toggle('icon-plus-sign');
	}

	function toggleColor(element,id,name) {
		a = element.classList.toggle('btn-success');
		b = element.classList.toggle('btn-danger');
		e = document.getElementById(name+"_"+id);
		if (e!=null) document.getElementById(name+"_"+id).style.display = b2show[a];
		setOption(id,{key:name,value:b2i[a]});
	}



	// --------------------------- api -----------------------------




	function ajax(method,callback,values) {
		document.body.style.cursor = 'progress';
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
				document.body.style.cursor = 'pointer';
    		console.log(this.response);
    		if (callback!=null) callback(JSON.parse(this.response));
	    }
	  };
		input = "";
		for ( var key in values ) input += key+"="+values[key]+"&";
    url = "http://localhost/redweb/joomla7/administrator/index.php?option=com_aesir_sync&task=sync."+method+"&"+input, 
	  xhttp.open("GET",url,true);
	  xhttp.send();
	}

  function setOption(id,values,callback) {
  	values["id"] = id;
  	values["method"] = "setOption";
  	if ( values.key == "url" ) callback = loadXML(id);
  	ajax('run',callback,values);

  }

  function loadXML(id) {
    console.log(id);
	  ajax('run',refreshTree,{
	  	'method':'loadXML',
	  	'id':id,
	  	'content_type':document.getElementById('content_type_'+id).value,
	  	'name':document.getElementById('url_name_'+id).innerText,
	  	'url':btoa(document.getElementById('url_path_'+id).innerText)
	  });
  }

  function setMethods(id,values) {
  	values["id"] = id;
  	values["method"] = "setMethods";
  	ajax('run',null,values);
  }

  function refreshTree(data) {
  	document.getElementById('com_aesir_sync').innerHTML = showTree(data,0,{html:""},{});
  }

  function addXML(id) {
	  ajax('run',refreshTree,{'method':'addXML','id':id});
  }

  function deleteXML(id) {
  	document.getElementById("element_"+id).style.display = "none";
  	document.getElementById("subtree_"+id).style.display = "none";
	  ajax('run',null,{'method':'deleteXML','id':id});
  }

  function syncAll() {
	  ajax('run',null,{'method':'syncAll'});
  }

	function addCron(id) {
		ajax('run',null,{
			'id':id,
			'method':'addCron',
			'datetime':document.getElementById('datetime_'+id).value,
			'datetime_repeat':document.getElementById('datetime_repeat_'+id).value
		})
	}

	function removeCron(id) {
		ajax('run',null,{'id':id,'method':'removeCron'})
	}

  function setPercent(percent) {

  }

// ----------------------------------------------------------------  

	function test(id) {
		ajax('run',null,{'id':id,'method':'getProgress'})
	}



  ajax('run',refreshTree,{'method':'getTree'});







