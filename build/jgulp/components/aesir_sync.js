/**
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */
var gulp = require('gulp');
var config = require('../../gulp-config.json');

// Dependencies
var browserSync = require('browser-sync');
var del         = require('del');
var less        = require('gulp-less');
var minifyCSS   = require('gulp-minify-css');
var rename      = require('gulp-rename');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');

var componentName = 'com_aesir_sync';
var baseTask  = 'components.aesir_sync';

var extPath   = './../extensions';

var assetsPath = './src/assets/' + componentName;
var assetsVendorPath = './src/assets/vendor';
var mediaPath = extPath + '/media/com_aesir_sync';
var nodeModulesPath  = './node_modules';

var wwwMediaPath = config.wwwDir + '/media/' + componentName;

// Clean
gulp.task('clean:' + baseTask,
    [
        'clean:' + baseTask + ':backend',
        'clean:' + baseTask + ':media',
        'clean:' + baseTask + ':cli',
        'clean:' + baseTask + ':webservices'
    ],
    function() {
        return true;
    }
);

// Clean: backend
gulp.task('clean:' + baseTask + ':backend', function(cb) {
    del(config.wwwDir + '/administrator/language/**/*.com_aesir_sync.*', {force: true});

    return del(config.wwwDir + '/administrator/components/com_aesir_sync', {force : true});
});

// Clean: cli
gulp.task('clean:' + baseTask + ':cli', function(cb) {
    return del(config.wwwDir + '/cli/com_aesir_sync', {force : true});
});

// Clean: webservices
gulp.task('clean:' + baseTask + ':webservices', function(cb) {
    return del(config.wwwDir + '/media/redcore/webservices/com_aesir_sync', {force : true});
});

// Clean: media
gulp.task('clean:' + baseTask + ':media', function(cb) {
    return del(
        [
            config.wwwDir + '/media/com_aesir_sync/**',
            '!' + config.wwwDir + '/media/com_aesir_sync/images'
        ],
        {force : true}
    );
});

// Copy
gulp.task(
    'copy:' + baseTask,
    [
        'clean:' + baseTask,
        'copy:' + baseTask + ':backend',
        'copy:' + baseTask + ':media',
        'copy:' + baseTask + ':cli',
        'copy:' + baseTask + ':webservices'
    ],
    function() {
    }
);

// Copy: backend
gulp.task('copy:' + baseTask + ':backend', ['clean:' + baseTask + ':backend'], function(cb) {
    return (
        gulp.src(
            [
            extPath + '/component/admin/**'
            ]
        )
            .pipe(gulp.dest(config.wwwDir + '/administrator/components/com_aesir_sync')) &&
        gulp.src(extPath + '/aesir_sync.xml')
            .pipe(gulp.dest(config.wwwDir + '/administrator/components/com_aesir_sync')) &&
        gulp.src(extPath + '/install.php')
            .pipe(gulp.dest(config.wwwDir + '/administrator/components/com_aesir_sync'))
    );
});

// Copy: media
gulp.task('copy:' + baseTask + ':media', ['clean:' + baseTask + ':media'], function() {
    return gulp.src(mediaPath + '/**').pipe(gulp.dest(config.wwwDir + '/media/com_aesir_sync'));
});

gulp.task('copy:' + baseTask + ':cli', ['clean:' + baseTask + ':cli'], function() {
    return gulp.src(extPath + '/cli/**').pipe(gulp.dest(config.wwwDir + '/cli/com_aesir_sync'));
});

/** Copy webservices **/
gulp.task('copy:' + baseTask + ':webservices', ['clean:' + baseTask + ':cli'], function() {
    return gulp.src(extPath + '/webservices/**').pipe(gulp.dest(config.wwwDir + '/media/redcore/webservices/com_aesir_sync'));
});

// Watch
gulp.task('watch:' + baseTask,
    [
        'watch:' + baseTask + ':backend',
        'watch:' + baseTask + ':media',
        'watch:' + baseTask + ':cli',
        'watch:' + baseTask + ':webservices'
    ],
    function() {
        return true;
    }
);

function reload()
{
    //setTimeout(browserSync.reload, 1000);
}

// Watch: backend
gulp.task('watch:' + baseTask + ':backend', function() {
    gulp.watch(
        [
            extPath + '/component/admin/**/*',
            extPath + '/aesir_sync.xml',
            extPath + '/install.php'
        ],
        ['copy:' + baseTask + ':backend', reload]
    );
});

// Watch: media
gulp.task('watch:' + baseTask + ':media', function() {
    gulp.watch([mediaPath + '/**/*'],['copy:' + baseTask + ':media', reload]);
});

// Watch: cli
gulp.task('watch:' + baseTask + ':cli', function() {
    gulp.watch([extPath + '/cli/**/*'],['copy:' + baseTask + ':cli', reload]);
});

// Watch: webservices
gulp.task('watch:' + baseTask + ':webservices', function() {
    gulp.watch([extPath + '/webservices/**/*'],['copy:' + baseTask + ':cli', reload]);
});